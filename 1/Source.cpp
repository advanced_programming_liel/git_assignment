#include <iostream>
#include <string>
using namespace std;

#define BIGGER -1
#define SMALLER 1
#define EQUELL 0

template <class T>
int compare(T num1, T num2)
{
	if (num1 > num2)
	{
		return BIGGER;
	}
	else if (num1 < num2)
	{
		return SMALLER;
	}
	return EQUELL;
} 

template <class T>
void bubbleSort(T* arr, int n)
{
	T swap;
	for (int c = 0; c < (n - 1); c++)
	{
		for (int d = 0; d < n - c - 1; d++)
		{
			if (arr[d] > arr[d + 1]) /* For decreasing order use < */
			{
				swap = arr[d];
				arr[d] = arr[d + 1];
				arr[d + 1] = swap;
			}
		}
	}
}

template <class T>
void printArray(T* arr, int n)
{
	for (int i = 0; i < n; i++)
	{
		cout << arr[i] << " ";
	}
	cout << endl;
}

void main()
{
	int ans1 = ::compare(2, 1);

	cout << ans1 << endl;

	int arr1[6] = { 54,0,100,90,-1,0 };

	::printArray(arr1, 6);
	::bubbleSort(arr1, 6);
	::printArray(arr1, 6);



	int ans2 = ::compare("liel", "lielalbilya");

	cout << ans2 << endl;

	float arr2[6] = { 54.1,0.5,100.98,90.2,-1.1,0.1 };

	::printArray(arr2, 6);
	::bubbleSort(arr2, 6);
	::printArray(arr2, 6);



	int ans3 = ::compare(1.1, 1.2);

	cout << ans3 << endl;

	string arr3[6] = { "liel", "pop", "push", "minion", "cpp", "iwanttosleep"};

	::printArray(arr3, 6);
	::bubbleSort(arr3, 6);
	::printArray(arr3, 6);

	system("PAUSE");
}