#include "BSNode.h"
#include <exception>
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

template <class T>
class BSNode
{
public:
	BSNode(T data);
	BSNode(const BSNode<T>& other);

	virtual ~BSNode();

	virtual BSNode<T>* insert(T value);
	BSNode& operator=(const BSNode<T>& other);

	bool isLeaf() const;
	T getData() const;
	BSNode<T>* getLeft() const;
	BSNode<T>* getRight() const;

	bool search(T val) const;

	int getHeight() const;

protected:
	T _data;
	BSNode<T>* _left;
	BSNode<T>* _right;
};

template<class T>
BSNode::BSNode(string data) : _data(data), _left(0), _right(0), _count(1)
{
}

template<class T>
BSNode::BSNode(const BSNode& other)
{
	*this = other;
}

template<class T>
BSNode::~BSNode()
{
	//Recursive deletion
	if (_left)
	{
		delete _left; // will call the d'tor of left son
	}

	if (_right)
	{
		delete _right; // will call the d'tor of the right son
	}
}

template<class T>
bool BSNode::isLeaf() const
{
	return (_left == 0 && _right == 0);
}

template<class T>
BSNode<T>* BSNode::insert(string value)
{
	// check where should insert the node - right or left
	if (search(value))
	{
		this->_count++;
		return this;
	}
	else if (value <= this->_data)
	{
		if (_left) // if there is already left son
		{
			_left->insert(value); // recursive call on the left son
		}
		else
		{
			_left = new BSNode(value); // add the node as left son
		}
	}
	else 
	{
		if (_right) // if there is already right son
		{
			_right->insert(value); // recursive call on the right son
		}
		else
		{
			_right = new BSNode(value); // add the node as right son
		}
	}
	
	return this;
}

template<class T>
BSNode& BSNode::operator=(const BSNode& other)
{
	if (_left)
	{
		delete _left;
	}
	if (_right)
	{
		delete _right;
	}
	_data = other._data;
	_left = 0;
	_right = 0;

	//deep recursive copy
	if (other._left)
	{
		_left = new BSNode(*other._left);
	}

	if (other._right)
	{
		_right = new BSNode(*other._right);
	}
	_count = other._count;
	return *this;
}

template<class T>
bool BSNode::search(string val) const
{
	bool retVal = false;

	// if the current node is with the value
	if (_data == val)
	{
		retVal = true;
	}
	else if (_data >= val) // check if sould go right or left
	{
		if (_left)
		{
			retVal = _left->search(val); // recursive call on left son
		}
	}
	else if (_right)
	{
		retVal = _right->search(val); // recursive call on right son
	}

	return retVal;
}

template<class T>
void BSNode::printNodes() const
{	
	if (_left)
	{
		_left->printNodes();
	}

	cout << _data << " " << _count << endl;

	if (_right)
	{
		_right->printNodes();
	}

}

template<class T>
int BSNode::getHeight() const
{
	int retVal;
	
	// if leaf the height is 1
	if (isLeaf())
	{
		retVal = 1;
	}
	else
	{

		int rightHeight = 0;
		int leftHeight = 0;

		// calculate the height of left sub tree (recursive call)
		if (_left)
		{
			leftHeight = _left->getHeight();
		}

		// calculate the height of right sub tree (recursive call)
		if (_right)
		{
			rightHeight = _right->getHeight();
		}

		// check which sub tree is higher
		retVal = leftHeight + 1;
		if (rightHeight > leftHeight)
		{
			retVal = rightHeight + 1;
		}

	}

	return retVal;

}

template<class T>
string BSNode::getData() const
{
	return _data;
}

template<class T>
BSNode* BSNode::getLeft() const
{
	return _left;
}

template<class T>
BSNode* BSNode::getRight() const
{
	return _right;
}